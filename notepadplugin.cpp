﻿// notepadplugin.cpp
#include <qqml.h>
#include <QQmlEngine>
#include "notepadplugin.h"
#include "notepadio.h"

// 注册单例函数
static QObject* NotepadIOCreateCallback(
        QQmlEngine* qmlEngine,
        QJSEngine* jsEngine )
{
    Q_UNUSED( qmlEngine );
    Q_UNUSED( jsEngine );

    NotepadIO* io = NotepadIO::instance( );
    qmlEngine->setObjectOwnership( io, QQmlEngine::CppOwnership );
    return io;
}


NotepadPlugin::NotepadPlugin( QObject* parent ):
    QQmlExtensionPlugin( parent )
{
#if defined( QT_STATIC )
#if QT_VERSION >= QT_VERSION_CHECK( 5, 7, 0 )
    Q_INIT_RESOURCE( qmake_QtDream_Notepad );
#else
    Q_INIT_RESOURCE( notepadplugin );
#endif
#endif
    Q_INIT_RESOURCE( notepadassets );// 所有资源一定要打在一个包中，要不然程序会发现还是找不到相应的资源
}

void NotepadPlugin::registerTypes( const char* uri )
{
    Q_UNUSED( uri );
    qmlRegisterSingletonType<NotepadIO>( uri, 0, 3, "NotepadIO",
                                         NotepadIOCreateCallback );
}
