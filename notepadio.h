#ifndef NOTEPADIO_H
#define NOTEPADIO_H

#include <QUrl>
#include <QObject>

class NotepadIO : public QObject
{
    Q_OBJECT
public:
    explicit NotepadIO( QObject* parent = Q_NULLPTR );

    Q_INVOKABLE bool writeFile( const QByteArray& text,
                                const QUrl& url );
    static NotepadIO* instance( void );
signals:

public slots:
};

#endif // NOTEPADIO_H
