#include <QFile>
#include "notepadio.h"

NotepadIO::NotepadIO( QObject* parent ): QObject( parent )
{

}

bool NotepadIO::writeFile( const QByteArray& text,
                           const QUrl& url )
{
    QFile file( url.toLocalFile( ) );
    if ( file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        file.write( text );
        file.close( );
        return true;
    }
    return false;
}

NotepadIO* NotepadIO::instance( void )
{
    static NotepadIO io;
    return &io;
}
