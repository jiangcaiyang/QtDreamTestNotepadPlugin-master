﻿// notepadglobal.h
#ifndef NOTEPADGLOBAL_H
#define NOTEPADGLOBAL_H

#include <qglobal.h>
#include <QLoggingCategory>

#if defined( NOTEPAD_LIB )
#   define NOTEPAD_EXPORT Q_DECL_EXPORT
#elif defined( QT_STATIC )
#   define NOTEPAD_EXPORT
#else
#   define NOTEPAD_EXPORT Q_DECL_IMPORT
#endif

Q_DECLARE_LOGGING_CATEGORY( QTDREAM_NOTEPAD )

#endif// NOTEPADGLOBAL_H
