# notepad.pro
QT += qml quick
DEFINES += NOTEPAD_LIB

OTHER_FILES += notepadplugin.json .qmake.conf

QMAKE_TARGET_PRODUCT = "QtDream Notepad Module (Qt $$QT_VERSION)"
QMAKE_TARGET_DESCRIPTION = "QtDream notepad module, tekes a notepad easily."

lupdate_only {
    SOURCES =   qml/*.qml \
                qml/*.js
}

qtHaveModule( qml ) {

    HEADERS += notepadplugin.h
    SOURCES += notepadplugin.cpp
    DISTFILES += qmldir

    !greaterThan( QT_MINOR_VERSION, 6 ) {
        RESOURCES += notepadplugin.qrc
    }

    CXX_MODULE = dreamnotepad
    TARGET  = qtdreamnotepad
    TARGETPATH = QtDream/Notepad
    IMPORT_VERSION = 0.2
    load( qml_plugin )
}

RESOURCES +=    notepadassets.qrc

# 这些是文档的部分
#QMAKE_DOCS = $$PWD/doc/notepad.qdocconf
#OTHER_FILES += $$QMAKE_DOCS doc/src/*.qdoc

HEADERS +=  notepadglobal.h \
    notepadio.h
SOURCES +=  notepadglobal.cpp \
    notepadio.cpp
