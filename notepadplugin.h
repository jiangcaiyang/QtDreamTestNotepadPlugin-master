﻿// notepadplugin.h
#ifndef NOTEPADPLUGIN_H
#define NOTEPADPLUGIN_H

#include <QQmlExtensionPlugin>

class NotepadPlugin: public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA( IID QQmlExtensionInterface_iid FILE "notepadplugin.json" )
public:
    NotepadPlugin( QObject* parent = Q_NULLPTR );
    void registerTypes( const char* uri );
};

#endif // NOTEPADPLUGIN_H
