﻿import QtQuick 2.10
import QtQuick.Controls 1.4
//import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import QtDream.Core 3.2
import QtDream.Controls 1.9
import QtDream.Notepad 0.3
import QtQuick.Dialogs 1.2 as MyDialogs


Page
{
    id: page
    implicitWidth: 270
    implicitHeight: 480

    Component.onCompleted: {
    }

    property string title: ""

    //菜单
    menuBar: MenuBar
    {
        Menu
        {
            title: qsTr( "文件" ) + Translator.notifier

            MenuItem {
                text: "新建(N)"
                shortcut: ("Ctrl+N")
                onTriggered: {
                    console.log("click " + text)
                    funcNew()
                }
            }
            MenuItem {
                text: "打开(O)"
                shortcut: ("Ctrl+O")
                onTriggered: {
                    console.log("click " + text)
                }
            }
            MenuItem {
                text: "保存(S)"
                shortcut: ("Ctrl+S")
                onTriggered: {
                    console.log("click " + text)
                }
            }
            MenuItem {
                text: "另存为(A)"
                shortcut: ("Ctrl+A")
                onTriggered: {
                    console.log("click " + text)
                }
            }
        }
    }

    function funcNew(){
        if(textEdit.text != "" && title == ""){
            //提示保存
            messageDialogNew.open()
        }
    }

    function funcSaveAs(){}

    function funcWriteFile(text, path){
        NotepadIO.writeFile( text, path );
    }


    //主界面
    Item{
        id: item
        width: parent.width
        height: parent.height-y
        y: 22

        //背景 纯色
        Rectangle{
            anchors.fill: parent
            color: "white"
            opacity: 1
        }

        ScrollView{
            id: scrollView
            anchors.fill: parent

            TextEdit{
                id: textEdit
                width: scrollView.width-25
                selectByMouse: true
                focus: true
            }
        }
    }

    //提示框
    MyDialogs.MessageDialog{
        id: messageDialogNew
        visible: false
        title: "记事本"
        text: "是否将更改保存到 无标题"

        standardButtons: MyDialogs.StandardButton.Save | MyDialogs.StandardButton.Discard | MyDialogs.StandardButton.Cancel
        onAccepted: {
            console.log("save")
            fileDialog.open()
        }
        onDiscard: {
            console.log("Discard")
        }
        onRejected: {
            console.log("Rejected")
        }
    }

    //对话框
    MyDialogs.FileDialog {
        id: fileDialog
        visible: false
        title: "另存为"
        folder: shortcuts.home
        selectExisting: false
        nameFilters: [ "文本文档 (*.txt)", "所有文件 (*.*)" ]
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrl)
            funcWriteFile(textEdit.text, fileDialog.fileUrl)
        }
        onRejected: {
        }
    }

}
